package comman_utility_package;
import java.io.File;



import java.io.FileWriter;
import java.io.IOException;


public class Handle_API_Logs {
	public static File Create_log_Directory(String DirectoryName) {
		String Current_Project_Directory = System.getProperty("user.dir");
		System.out.println(Current_Project_Directory);
		File Log_Directory = new File(Current_Project_Directory + "\\API_Log\\" + DirectoryName);
		Delete_Directory(Log_Directory);
		Log_Directory.mkdir();
        return Log_Directory;
	}

	public static boolean Delete_Directory(File Directory) {
		boolean Directory_deleted = Directory.delete();
		if (Directory.exists()) {
			File[] files = Directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						Delete_Directory(file);
					} else {
						file.delete();
					}
				}
			}
			Directory_deleted = Directory.delete();
		}

		return Directory_deleted;
	}

	public static void evidence_creator(File dirname, String filename, String endpoint, String requestBody,
			String responseBody) throws IOException {

		// Step 1 create the file at given location
		File newfile = new File(dirname + "\\" + filename + ".txt");
		System.out.println("new file created to save evidence:" + newfile.getName());

		// step 2 write data into the file

		FileWriter dataWriter = new FileWriter(newfile);
		dataWriter.write("End point : " + endpoint + "\n\n");
		//dataWriter.write("Request Body:\n\n" + requestBody + "\n\n");
		dataWriter.write("Response Body:\n\n" + responseBody);
		dataWriter.close();
		System.out.println("EVIDENCE IS WRITTEN IN FILE :" + newfile.getName());

	}




	
	}


