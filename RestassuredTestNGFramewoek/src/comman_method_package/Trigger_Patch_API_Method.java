package comman_method_package;
import static io.restassured.RestAssured.given;
public class Trigger_Patch_API_Method {

	public static int extract_Status_Code(String req_Body,String URL) {
		   int StatusCode = given().header("Content-Type","application/json").body(req_Body)
				   .when().patch(URL).then().extract().statusCode();
		   return StatusCode;
				   
	}
public static String extract_Response_Body(String req_Body, String uri) {
	String responseBody = given().header("Content-Type","application/json").body(req_Body)
			              .when().patch(uri)
			              .then().extract().response().asString();
	return responseBody;
			
}
}
	


